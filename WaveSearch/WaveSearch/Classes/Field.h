#ifndef __WaveSearch__Field__
#define __WaveSearch__Field__

#include <iostream>


#include "cocos2d.h"
#include "Cell.h"
using namespace cocos2d;
using namespace std;

//Типы окрестностей будем использовать
typedef enum
{
    Moor, // Окрестности Мура
    Neumann // Окрестности Фон Неймана
} Neighborhood;

class Field : public CCLayer
{
private:
    static const char neighLeftDelta = -1;
    static const char neighRightDelta = 2;
    static const char wallProb = 2;

public: //ф-ции стандартной инициализации
    CREATE_FUNC(Field);
    virtual bool init();
    static CCScene* scene();
    
private: //поля данных
    int _fieldColumnsCount = 0;
    int _fieldRowsCount = 0;
    int _col = 0;
    int _row = 0;
    
    vector<Cell*> _route;
    Cell * _beginCell;
    Cell * _endCell;
    vector< vector< Cell* > > _cells;

public: //ф-ции генерации поля
    static bool steteIsWall();
    void generateField();
    void regenerateField();
    bool createRandomCell(int col, int row);
    bool updateRandomCell(int col, int row);

public: //accessors
    float getRowsCount();
    float getColumnsCount();
    void setColumnsCount( float fieldWidth );
    void setRowsCount( float fieldHeight );

public: // Обработчики нажатий
    virtual void ccTouchesBegan( CCSet * touches, CCEvent * event ){};
    virtual void ccTouchesMoved( CCSet * touches, CCEvent * event ){};
    virtual void ccTouchesEnded( CCSet * touches, CCEvent * event );
    
public://ф-ции шагов основного алгоритма
    void makeWave();
    void restoreRoute();
    bool evaluateRoute();

public://ф-ции проверок
    Cell * checkBlock(int col, int row);
    bool validFieldCoord( int col, int row );
    static bool validNeighborhood( const int col, const int row, Neighborhood vicinity );
    
public://ф-ции обхода
    template <class object>
    void iterateOverNeighbors(object *obj, bool (object::*cellFunc)(int, int, int), const int curCol, const int curRow, const int step);

    template <class object>
    void iterateOverField(object *obj, bool (object::*cellFunc)(int,int));
    
public://калбеки ф-й обхода
    bool markCell(int col, int row, int step);
    bool checkNearestCell(int col, int row, int step);

public: //показ стандартного диалога iOS
    void showDialog(const string title, const string message, const string okMessage);
};

#endif