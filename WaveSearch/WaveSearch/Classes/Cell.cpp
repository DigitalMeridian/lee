#include "Cell.h"

float Cell::_cellWidth = 20.0f;
float Cell::_cellHeight = 20.0f;


void Cell::hideAllStates()
{
    CCObject* obj;
    CCARRAY_FOREACH(getChildren(),obj)
    {
        //do something. static cast up to do more
        static_cast<CCSprite*>(obj)->setVisible(false);
    }
}

void Cell::setState(CellState state)
{
    _currentState = state;
    hideAllStates();
    switch (state) {
            
        case EmptyState:
            _normalState->setVisible(true);
            break;
            
        case RouteState:
            _routeState->setVisible(true);
            break;
            
        case EndState:
            _endState->setVisible(true);
            break;
            
        case WallState:
            _wallState->setVisible(true);
            break;
            
        default:
            _normalState->setVisible(true);;
    }
}

CellState Cell::getState()
{
    return _currentState;
}

bool Cell::isWall()
{
    return _currentState == WallState;
}


void Cell::resetRouteParams()
{
    _step = -1;
}

int Cell::getStep()
{
    return _step;
}

void Cell::setStep(int step)
{
    _step = step;
}

bool Cell::isMarked()
{

    return _step != -1;
}


void Cell::setCellWidth(float cellWidth)
{
    _cellWidth = cellWidth;
}

float Cell::getCellWidth()
{
    return _cellWidth;
}

void Cell::setCellHeight(float cellHeight)
{
    _cellHeight = cellHeight;
}

float Cell::getCellHeight()
{
    return _cellHeight;
}

int Cell::getSheetCol()
{
    return _col;
}

int Cell::getSheetRow()
{
    return _row;
}

void Cell::setSheetPos(int col, int row)
{
    _col = col;
    _row = row;
    setPosition( ccp(_cellWidth * col, _cellHeight * row) );
}

void Cell::initCell()
{
    _step = -1;
    setAnchorPoint(ccp(0, 0));
    _normalState = CCSprite::create("normal.png");
    _wallState = CCSprite::create("wall.png");
    _routeState = CCSprite::create("way.png");
    _endState = CCSprite::create("way_end.png");
    
    _normalState->setAnchorPoint(CCPointZero);
    _wallState->setAnchorPoint(CCPointZero);
    _routeState->setAnchorPoint(CCPointZero);
    _endState->setAnchorPoint(CCPointZero);
    
    addChild(_normalState);
    addChild(_wallState);
    addChild(_routeState);
    addChild(_endState);
    setState(EmptyState);
}