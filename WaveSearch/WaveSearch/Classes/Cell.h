#ifndef __WaveSearch__Cell__
#define __WaveSearch__Cell__

#include <iostream>
#include <cocos2d.h>
#include "CCLabelTTF.h"
using namespace cocos2d;

typedef enum {
    EndState = 0,
    WallState = 1,
    EmptyState = 2,
    RouteState = 3
} CellState;

class Cell: public CCSprite {

private:
    CellState _currentState;
    int _col = 0;
    int _row = 0;
    int _step = -1;
    static float _cellWidth;
    static float _cellHeight;

    CCSprite* _normalState;
    CCSprite* _wallState;
    CCSprite* _routeState;
    CCSprite* _endState;
public:
    CREATE_FUNC(Cell);
    bool isMarked();

    bool isWall();
    
    
    void setState(CellState state);
    CellState getState();

    
    static void setCellWidth( float cellWidth );
    static float getCellWidth();
    
    static void setCellHeight( float cellHeight);
    static float getCellHeight();

    void initCell();
    void setSheetPos( int col, int row );

    int getSheetCol();
    int getSheetRow();
    
    void setStep(int step);
    int getStep();

    
    void resetRouteParams();
    void hideAllStates();
};

#endif
