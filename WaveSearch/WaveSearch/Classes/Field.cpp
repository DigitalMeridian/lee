#include "Field.h"    
#include <math.h>
#include "CCEGLView.h"
#include "SimpleAudioEngine.h" 

using namespace std;
using namespace CocosDenshion;


float Field::getRowsCount()
{
    return _fieldRowsCount;
}

float Field::getColumnsCount()
{
    return _fieldColumnsCount;
}

void Field::setColumnsCount(float fieldHeight)
{
    _fieldColumnsCount = fieldHeight;
}

void Field::setRowsCount(float fieldWidth)
{
    _fieldRowsCount = fieldWidth;
}

CCScene* Field::scene()
{
    CCScene *scene = CCScene::create();
    Field *layer = Field::create();
    scene->addChild( layer );
    return scene;
}

//Рандомно выставляем стены на карте
bool Field::steteIsWall()
{
    int min = 0;
    int max = 10;
    int output = min + ( rand() % ( int )( max - min + 1 ) );
    if (output <= wallProb ) //шанс что текущая ячейка будет - стеной
        return true;
    return false;
}

//можно ли помечать шагом соседа
bool Field::validNeighborhood( const int col, const int row, Neighborhood vicinity )
{
    //сами мы не можем быть помечены
    if (col == 0 && row == 0)
        return false;
    //если окрестности Мура - выбрасываем диагональные точки
    if (vicinity == Moor)
    {
        if ((col == -1 && row ==  0) ||
            (col ==  1 && row ==  0) ||
            (col ==  0 && row == -1) ||
            (col ==  0 && row ==  1))
            return true;
    }
    //если окрестности не Мура то все кроме самих себя есть валидная ячейка
    return false;
}

//не вышли ли за границы поля
bool Field::validFieldCoord( int col, int row )
{
    if ( ( col ) < 0 || ( row ) < 0 )
        return false;
    
    if ( ( col ) >= _fieldColumnsCount ||
         ( row ) >= _fieldRowsCount )
        return false;
    
    return true;
}

//обходим все соседние ячейки
template <class object>
void Field::iterateOverNeighbors(object *obj, bool (object::*cellFunc)(int,int,int), const int curCol, const int curRow, const int step)
{
    int tmpCol = 0;
    int tmpRow = 0;
    
    for ( int col = neighLeftDelta; col < neighRightDelta; col++ )
    {
        for ( int row = neighLeftDelta; row < neighRightDelta; row++ )
        {
            tmpCol = curCol + col;
            tmpRow = curRow + row;
            
            //проверим пренадлежит ли точка требуемой окрестности
            if ( !validNeighborhood( col, row, Moor ) )
                continue;
            
            if (!validFieldCoord(tmpCol, tmpRow))
                continue;
            // если не надо продолжать итерацию по всем соседям
            if (!((obj)->*(cellFunc))(tmpCol, tmpRow, step)) return;
        }
    }
}

bool Field::markCell(int col, int row, int step)
{
    Cell* cell = (Cell *) _cells[ col ][ row ];
    
    if (cell->isWall())
        return true;
    
    if (!cell->isMarked())
        cell->setStep(step);
    return true;
}

void Field::makeWave()
{
    _beginCell->setStep(0);
    bool stopEval = false;
    int stepCouner = 0;
    do {
        stopEval = true;
        for (int col = 0 ; col< _fieldColumnsCount; col++)
            for (int row = 0; row< _fieldRowsCount; row++)
            {
                if (_cells[col][row]->getStep() == stepCouner)
                {
                    stopEval = false;
                    iterateOverNeighbors(this, &Field::markCell, col, row, stepCouner + 1);
                }
            }
        stepCouner ++;
    } while ( !stopEval && !_endCell->isMarked() );
}

bool Field::evaluateRoute()
{
    
    makeWave();
    bool marked = _endCell->isMarked();
    if (!marked) return false;
    restoreRoute();
    for(auto it = _route.begin(); it != _route.end(); it++)
    {
        Cell* cell = (*it);
        if (cell != _beginCell && cell != _endCell)
            cell->setState(RouteState);
    }
    return true;
}

//проверяем ячейку на то что в ней сидит шаг step-1 чтобы перейти в нее
bool Field::checkNearestCell(int col, int row, int step)
{
    Cell * cell = _cells[col][row];
    if (cell->getStep() == step)
    {
        _col = cell->getSheetCol();
        _row = cell->getSheetRow();
        _route.push_back(cell);
        return false;
    }
    return true;
}

void Field::restoreRoute()
{
    //Заносим конечную точку в начало маршрута
    _route.push_back(_endCell);
    //Запоминаем ее позицию
    _col = _endCell->getSheetCol();
    _row = _endCell->getSheetRow();
    int step = _endCell->getStep();
    while ( step > 0 )
    {
        step--;
        //обходим всех соседей и ищем там значение step-1
        iterateOverNeighbors(this, &Field::checkNearestCell, _col, _row, step);
    }
}

//ф-я обхода всех ячеек на поле
template <class object>
void Field::iterateOverField(object *obj, bool (object::*cellFunc)(int,int))
{
    for ( int col = 0; col < _fieldColumnsCount; col++ )
        for (int row = 0; row < _fieldRowsCount; row++)
            // если не надо продолжать итерацию
            if (!((obj)->*(cellFunc))(col, row)) return;
}

//обновляем поле
void Field::regenerateField()
{
    _route.clear();
    srand( time( NULL ) );
    iterateOverField(this, &Field::updateRandomCell);
}

//заново генерируем параметры ячейки для обновления поля
bool Field::updateRandomCell(int col, int row)
{
    Cell * tmpCell = _cells[col][row];
    tmpCell->resetRouteParams();
    tmpCell->setState( steteIsWall() ? WallState: EmptyState );
    return true;
}

//создаем ячейку на поле и позиционируем ее
bool Field::createRandomCell(int col, int row)
{
    Cell * pCell = ( Cell* ) Cell::create();
    pCell->initCell();
    pCell->setState( steteIsWall() ? WallState: EmptyState );
    _cells[col].push_back( pCell );
    this->addChild( pCell );
    pCell->setSheetPos( col, row );
    return true;
}

//создаем все ячейки на поле
void Field::generateField()
{
    _fieldColumnsCount  = CCEGLView::sharedOpenGLView()->getFrameSize().width / Cell::getCellWidth();
    _fieldRowsCount = CCEGLView::sharedOpenGLView()->getFrameSize().height /  Cell::getCellHeight();
    _cells.resize( _fieldColumnsCount );
    srand( time(NULL) );
    //обходим все поле размером _fieldColumnsCount x _fieldRowsCount и созаем там ячейки
    iterateOverField(this, &Field::createRandomCell);
}

bool Field::init()
{
    if ( !CCLayer::init() ) return false;
    CCDirector::sharedDirector()->getTouchDispatcher()->addStandardDelegate( this, 0 );
    //подгружаем не музыку
    SimpleAudioEngine::sharedEngine()->preloadEffect("wrong_end.mp3");
    SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("background.mp3");
    SimpleAudioEngine::sharedEngine()->playBackgroundMusic("background.mp3", true);
    //создаем поле после стандартного инишиала
    generateField();
    return true;
}



//Проверяем не препядствие ли перед нами,
//срабатывает только тогда когда происходит выбор
Cell * Field::checkBlock(int col, int row)
{
    Cell * tmpCell = (Cell *)_cells[col][row];
    //если да - ругаемся и не меняем текущее положение дел в задании точек
    if (tmpCell->isWall())
    {
        showDialog("Wow!","You can't choose white block! \n Please choose another.", "OK");
        return NULL;
    }
    tmpCell->setState(EndState);
    return tmpCell;
}

//вызов стандартного модального диалога iOS с одной кнопкой.
void Field::showDialog(const string title, const string message, const string okMessage)
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    CCApplication::sharedApplication()->showModalInfoDialog(title, message, okMessage);
    #endif
}

void Field::ccTouchesEnded( CCSet * touches, CCEvent * event )
{
    for(auto it = touches->begin(); it != touches->end(); it++)
    {
        auto touch = dynamic_cast<CCTouch*>(*it);
        if(touch == NULL)
            break;
        CCPoint pPoint = touch->getLocation();
        //Находим какой ячейке пренадлежит тычек
        int col = pPoint.x / Cell::getCellWidth();
        int row = pPoint.y / Cell::getCellHeight();

        if (_beginCell == NULL)
        {
            _beginCell = checkBlock(col, row);
        }
        else if (_endCell == NULL)
        {
            _endCell = checkBlock(col, row);
        }
        else
        {
            _endCell = NULL;
            _beginCell = NULL;
            regenerateField();
        }
        //ячейки начала и конца выбраны
        if (_endCell && _beginCell)
        {
            //пытаемся вычислить путь
            if (evaluateRoute())
            {
                //получилось - показываем что нашли
                char text[256];
                sprintf( text, "Length : %lu cell(s)", _route.size() );
                showDialog( "Route found!", text, "OK" );
            }
            else
            {
                SimpleAudioEngine::sharedEngine()->playEffect("wrong_end.mp3");
                //не получилось - говорим об этом и сбрасываем точки
                showDialog( "Route not found! :(", "Choose another destination!", "OK" );
                //сбрасываем последнюю точку
                _endCell->setState(EmptyState);
                _endCell = NULL;
            }
        }

    }
}


